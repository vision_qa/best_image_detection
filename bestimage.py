import cv2
import glob
import os
import numpy as np
from matplotlib import pyplot 
import shutil

class bestimage:
    " Find image having highest quality"
    
    def __init__(self,img_path = './Images',best_img_path = './BestImage'):
        self.img_path = img_path
        if not os.path.exists(best_img_path):
            os.mkdir(best_img_path)
        self.best_img_path = best_img_path
        

    def standard_deviation(self,image):
        "Return std of image histogram"
        hist = cv2.calcHist(image,[0],None,[256],[0,256])
        #plt.hist(image.ravel(),256,[0,256]); plt.show()

        return np.std(hist)
    

    def best_histogram(self,images):
        " Return image with highest std in image histogram"     
        return [max(images, key = self.standard_deviation)]

    
    def find_brightness(self,image):
        """
        Return brightness value of an image.
        Brightness range from 0 to 1.
        Image having brightness near 0 is dark and near 1 is bright.
        """
        histogram = cv2.calcHist(image,[0],None,[256],[0,256])
        pixels = np.sum(histogram)
        brightness = scale = len(histogram)

        for index in range(0, scale):
            ratio = histogram[index] / pixels
            brightness += ratio * (-scale + index)

        return 1 if brightness == 255 else brightness / scale
    

    def well_illuminated_image(self,images):
        """
        Check if the image have proper illumination.
        Considering image having brightness between 0.45 and 0.6
        as properly illuminated .
        """
        phase2_imgs = [img for img in images
                       if  0.45 <= self.find_brightness(img) <= 0.6]
        img_count = len(phase2_imgs)
        print('Total images in phase 2:',img_count)
        
        if img_count > 0:
            return ( phase2_imgs if img_count == 1
                     else self.best_histogram(phase2_imgs) )
        

    def find_clear_image(self,images):
        """
        Return clear images.
        Check variance of laplacian to check bluriness.
        """
        return [img for img in images
                if cv2.Laplacian(img, cv2.CV_64F).var() >100]
        

    def find_best_image(self,images):
        """
        Return image satisfying blurriness and brightness criteria.
        Return None if no such image is found
        """
        phase1_imgs = self.find_clear_image(images)
        img_count = len(phase1_imgs)
        print('Total images in phase 1:', img_count)
        
        if img_count > 0:
            return ( phase1_imgs if img_count == 1
                     else self.well_illuminated_image(phase1_imgs))
            

    def save_best_image(self):
        """
        Find best image among other images of a directory.
        Save that image in another directory
        """
        path = os.path.join(self.img_path,'*.jpg')
        img_names = glob.glob(path)
        images = [cv2.imread(name,0) for name in img_names]
        best_img = self.find_best_image(images)
        if best_img is not None:
            best_img_index = (i for i,image in enumerate(images)
                                 if np.array_equal(image,best_img[0]))
            best_img_name = img_names[next(best_img_index)]
            
            img_path = os.path.join(self.best_img_path,os.path.basename(best_img_name))
            shutil.copyfile(best_img_name,img_path)
        else:
            print("Can't find good picture")
                

if __name__ == '__main__':

    a = bestimage()
    a.save_best_image()
    
    
    

    
    
