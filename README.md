# 					Best Image Detector

Copy best image from pool of images in a directory to **BestImage** directory based on some criteria. Doesn't copy any image if these criteria don't  meet.

#### Selection criteria:

- Check blurriness 
- Check brightness
- Check image histogram distribution

#### Requirements

- opencv
- numpy
- matplotlib (***optional*** required to visualize histogram)
- glob
- shutil

#### Example

This will copy best image from a directory to **BestImage** directory (default)

```python
import bestimage as bi
#Default image path is Images
image_path = '..'
img = bi.bestimage()
img.save_best_image()
```





